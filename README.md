# OSMC / Kodi CEC-controlled standby script

## Purpose

Some embedded computers like the Raspberry series do not support any standby 
mode nor can their power be controlled.

This means that Kodi continues to run in the background when the TV is 
switched off. This may not be a great deal in terms of power usage but can be
somewhat problematic when Kodi is using a PVR backend and keeps occupying one
of the few limited subscriptions.

This little script / Debian service solves this pragmatically by starting / 
stopping Kodi depending on the TV state.

This is done by continuously monitoring the TV state via CEC and 
starting/stopping the Kodi service, accordingly. This also works with Samsung
TVs.


## install

Simply run:

```
curl https://gitlab.com/jampy81/osmc-cec-standby/raw/master/install-from-gitlab.sh | sudo bash

``` 

Then `reboot` the device.

This will disable the `mediacenter` (Kodi) service and instead install a new 
service that will start/stop Kodi based on the TV status.