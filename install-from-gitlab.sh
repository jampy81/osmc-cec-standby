#!/bin/bash

set -e

base="https://gitlab.com/jampy81/osmc-cec-standby/raw/master/src/"

curl "$base/cec-standby.service" > /lib/systemd/system/cec-standby.service
curl "$base/cec-standby.sh" > /usr/bin/cec-standby.sh

chmod a+x /usr/bin/cec-standby.sh

# enable monitoring service
systemctl enable cec-standby

# start the service now
systemctl restart cec-standby


echo ""
echo "=== INSTALL OK ==="
