#!/bin/bash


fifo_fn=/tmp/cec-standby.fifo

if [ ! -e $fifo_fn ]; then
  mkfifo $fifo_fn
fi


kodi_is_running()
{
  if systemctl status mediacenter > /dev/null ; then
    return 0
  else
    return 1
  fi
}


kodi_pgrp() {

  cat /proc/$(pidof -s kodi.bin)/stat | cut -d " " -f 5

}

start_pvr() {

  echo "START KODI PVR..."
  kodi-send --action="Action(PlayPvrTV)"

}

stop_pvr() {

  echo "STOP KODI PVR..."
  kodi-send --action="PlayerControl(Stop)"

}


# power off can only be detected via CEC
monitor_cec() {

  # NB: direct `cec-client | grep` pipe doesn't work
  /usr/osmc/bin/cec-client | \
  while read line
  do

    pstate=$(echo "$line" | grep "TV.*power status changed from" | cut -d "'" -f 4)

    if [ "$pstate" = "standby" ]; then
      echo off > $fifo_fn
    fi

  done

}


# power on is *much* faster via HDMI
monitor_hdmi() {

  /opt/vc/bin/tvservice -M 2>&1 | \
  while read line
  do

    if echo "$line" | grep -qF "HDMI is attached" ; then
      echo on > $fifo_fn
    fi

  done

}


react_on_status() {

  while :; do

    cat $fifo_fn | \
    while read line ; do

      case "$line"
      in
        "on")
          echo "Current TV state: ON"
          start_pvr
          sleep 3  # debounce
          ;;

        "off")
          echo "Current TV state: OFF"
          stop_pvr
          sleep 3  # debounce
          ;;

      esac

    done

  done

}


echo "Entering loop..."
monitor_cec &
monitor_hdmi &
react_on_status
